public class BonusAttack extends Effect{

    String when;
    String roll;
    
    public BonusAttack(String when,String roll){
        this.when=when;
        this.roll=roll;
    }
    
    @Override
    public void resolve(){
        if ("fight".equals(this.when)){
            if ("wound".equals(this.roll))
            this.target.bonus_atks_fight_wound=true; /* other scenarios are out of scope */
        }
    }
    
    @Override
    public boolean hitRoll(Weapon weap) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean saveRoll(int mod) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int rollDamage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
