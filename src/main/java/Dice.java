import java.util.Random;

public class Dice {
    
    int number_of_rolls; /* or equivalently number of dice of the same kind */
    int [] pool; /* array of all possible values of a single die */
    private final Random rand = new Random();
    
    /* die with a single constant value */
    public Dice(int n){
        this.number_of_rolls=1;
        this.pool= new int[] {n};
    }
    
    public Dice(int n, int... args){
        this.number_of_rolls=n;
        this.pool=args;
    }
    
    public int roll(){
        int sum=0;
        for (int i=0 ; i<number_of_rolls ; i++){
            sum += pool[rand.nextInt(pool.length)];}
        return sum;
    };
    
    public int reroll1(){
        int rolled=this.roll();
        if (rolled==1) rolled=this.roll();
        return rolled;
    }
    
    public boolean rollAgainst(int value){
        return this.roll()>=value;
    };
    
    public boolean rerollAgainst(int target_value,int rerolled_value){
        int rolled=this.roll();
        if (rolled<=rerolled_value) rolled=this.roll();
        return rolled >= target_value;
    }
    
    public boolean rerollAgainst(int value){
        return this.rerollAgainst(value,6);
    }
    
    public boolean reroll1Against(int value){
        return this.rerollAgainst(value,1);
    }
    
    public boolean rollUnder(int value){
        return this.roll()<=value;
    }
}
