public interface CombatOutput {
    static Dice d3=new Dice(1,new int [] {1,2,3});
    static Dice d6=new Dice(1,new int [] {1,2,3,4,5,6});
    
    public int rollDamage ();
    public int rollDamage (Dice d);
}
