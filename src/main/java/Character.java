public class Character implements Psyker, CombatInteraction {
    
    /* Character sheet stats */
    String name;
    int points;
    int movement; 
    int ws;
    int bs;
    int strength;
    int toughness;
    int wounds;
    int attacks;
    int lead;
    int save;
    
    /* Psyker stats*/
    protected boolean Psyker=false;
    int manifest;
    int deny;
    Power[] powers_known;
    
    /* Miscellaneous */
    protected int current_hp;
    protected Character target;
    Weapon equip[] = new Weapon[20];
    Ability[] abilities;
    
    /* Passive bonuses achieved by abilities */
    boolean reroll1hit = false;
    boolean rerollhit =false;
    boolean reroll1wound= false;
    boolean rerollwound = false;
    int invul_save=0;
    boolean sacrifice = false;
    boolean bonus_atks_fight_wound = false;
    int bonusattacks=0;
    
    /* Temporary bonuses */
    int temp_invul_save=0;
    
    public Character(String nm, int pts, int[] sheet){
        if(sheet.length!=9)
            throw new IllegalArgumentException("character sheet with wrong length");
        else
        {
        this.name=nm;
        this.points=pts;
        this.movement=sheet[0];
        this.ws=sheet[1];
        this.bs=sheet[2];
        this.strength=sheet[3];
        this.toughness=sheet[4];
        this.wounds=sheet[5];
        this.attacks=sheet[6];
        this.lead=sheet[7];
        this.save=sheet[8];
        this.current_hp=this.wounds;
        }
    }
  
    /* Equipment methods */
    
    public void setEquip(Weapon... args){
        for (Weapon arg: args){
            if (arg instanceof RangedWeapon)
                this.lockAndLoad((RangedWeapon) arg);
            else 
                this.wield((MeleeWeapon) arg);
        }
    }
    
    public void lockAndLoad (RangedWeapon weap){
        for(int i=0;i<this.equip.length;i++){
            if(equip[i]==null){
                equip[i]=weap;
                break;
            }
        }
    }
  
    public void wield (MeleeWeapon weap){
        for(int i=0;i<this.equip.length;i++){
            if(equip[i]==null){
                equip[i]=weap;
                ((MeleeWeapon) equip[i]).number_of_attacks=this.attacks;
                ((MeleeWeapon) equip[i]).strength=((MeleeWeapon) equip[i]).strength_multiplier*this.strength +((MeleeWeapon) equip[i]).strength_bonus;
                break;
            }
        }
    }
    
    /* Psychic powers section*/
    
    @Override
    /* Needed for making a Psyker character */
    public void setPowers(int manifest, int deny, Power... known){
        this.Psyker=true;
        this.manifest=manifest;
        this.deny=deny;
        this.powers_known=known;
    };
    
    public void cast(Power p){
        
        /* Define target */
        if ("offensive".equals(p.category)) p.effect.target=this.target;
        else if ("buff".equals(p.category)) p.effect.target=this;
        
        System.out.println(this.name + " tries to cast " + p.name + "! ");
        
        /* Psychic test */
        int cast_roll = double_d6.roll();
        
        /* There's a chance to damage self and nearby characters */
        if (cast_roll == 2 || cast_roll == 12) {
            System.out.println(this.name + " suffers the Perils of the Warp!");
            p.perils.chooseTarget(this);
            p.perils.resolve();
            if (! this.isAlive()) { /* Perils damage other people only if Psyker dies */
                System.out.println("His adversary also suffers the Perils!");
                p.perils.chooseTarget(this.target);
                p.perils.resolve();
            }
        }
        
        /* Final result */
        if (this.isAlive() && (cast_roll >= p.warp_charge)) {
            System.out.println("It works!");
            p.effect.resolve();
        }
        else System.out.println("But it fails!");
    }

    /* Buff powers have a duration and must be cancelled */
    public void cancelBuffs(){
        for (Power p : this.powers_known)
            if ("buff".equals(p.category))
                {
                p.effect.target=this;
                p.effect.cancel();
                }
    }
    
    public boolean isPsyker(){
        return this.Psyker;
    }
    
    /* Abilties methods */
    
    public void setAbilities(Ability ...args){
        this.abilities=args;
    };
    
    /* Abilites must also be "cast" before the combat to make them have effect */
    public void loadAbilities(){
        for (Ability ab : this.abilities) {
            for (Effect effect : ab.effects){
                effect.chooseTarget(this);
                effect.resolve();
            }
        }
    }
    
    public void getAbilities(){
        for (Ability ab : this.abilities)
            System.out.println(ab.name);
    }
    
    /* Combat methods */
    
    public void isWounded(int dmg){
        this.current_hp -= dmg;
        if (this.current_hp <0) this.current_hp=0;
    }
  
    public boolean isAlive(){
        return this.current_hp >0;
    };
    
    public void restore(){
        this.current_hp=this.wounds;
    };
  
    @Override
    public void chooseTarget(Character c){
        this.target=c;
    }
  
    @Override
    /* First roll of an attack: character skill check */
    public boolean hitRoll(Weapon weap){
        
        /* ws for melee, bs for ranged */
        int skill;
        if (weap instanceof MeleeWeapon) skill=this.ws;
        else skill=this.bs;
        
        /* Apply the right type of roll from abilites */
        if (this.reroll1hit) return d6.reroll1Against(skill);
        else {
            if (this.rerollhit) return d6.rerollAgainst(skill);
            else return d6.rollAgainst(skill);
        }
    };
    
    /* Second roll of an attack: weapon strength vs target's toughness */
    public boolean woundRoll(Weapon weap){
        
        int target_value;
        
        /* Some sort of table would be good here */
        if (weap.strength >= 2*target.toughness) target_value =2;
        else if (weap.strength > target.toughness) target_value=3;
        else if (weap.strength == target.toughness) target_value=4;
        else if (2*weap.strength < target.toughness) target_value=6;
        else target_value=5;
        
        /* Apply the right type of roll from abilites */
        if (this.reroll1wound) return d6.reroll1Against(target_value);
        else {
            if (this.rerollwound) return d6.rerollAgainst(target_value);
            else return d6.rollAgainst(target_value);
        }
    }

    @Override
    /* Third roll of an attack: target's armor save */
    public boolean saveRoll(int ap) {
        
        /* Invulnerability and armor save don't stack, one must be chosen */
        /* Armor has better (i.e. lower) values but suffers from armor piercing */
        
        if (this.temp_invul_save > 0 && this.temp_invul_save < (this.save -ap)) {
            
            /* Apply a SHIELD tag if the invulnerability save roll is a success */
            if (d6.rollAgainst(this.temp_invul_save)){
                System.out.print("(SHIELD) ");
                return true;
            }
            else return false;
        }
        
        /* Armor save is chosen instead */
        else return d6.rollAgainst(this.save - ap);
    }
    
    /* The character cycles through all the attacks with a single weapon*/
    public void attacks (Weapon weap){
        for (int i=0 ; i < weap.number_of_attacks ; i++){
            if (this.target.isAlive()){
                this.attacksOnce(weap,false);
            }
            else break; /* Please don't beat a dead enemy */
        }
        
        /* Bonus attacks don't generate others and are calculated last */
        this.bonusAttacks(weap, this.bonusattacks);
        this.bonusattacks=0;
    }
    
    /* The character cycles through his extra/bonus attacks */
    public void bonusAttacks (Weapon weap, int extra){
        for (int i=0 ; i < extra ; i++){
            if (this.target.isAlive()){
                System.out.print("(BONUS) ");
                this.attacksOnce(weap,true);
            }
            else break;
        }
    }
    
    /* Method for a single attack, with distinction between bonus attacks and not */
    public void attacksOnce (Weapon weap, boolean bonus){
        System.out.print(this.name +" tries to attack with "+weap.name);
        if (this.hitRoll(weap)) {
            System.out.println(" and it hits! ");
            if (this.woundRoll(weap)) {
                
                /* This is when extra attacks are generated */
                if ((weap instanceof MeleeWeapon) && this.bonus_atks_fight_wound && (! bonus)) this.bonusattacks++;
                
                /* Save roll and damage calculation */
                if (! this.target.saveRoll(weap.ap)) {
                    int rolled_damage=weap.rollDamage();
                    System.out.println(this.target.name +" takes "+rolled_damage +" damage!");
                    this.target.isWounded(rolled_damage);
                }
                else System.out.println(this.target.name +" endures the attack!"); 
            }
            else System.out.println("But the attack doesn't make a scratch!");
        } 
        else System.out.println(" but it misses!");
    }
}
