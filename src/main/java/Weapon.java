public abstract class Weapon implements CombatOutput{
  
    protected String name;
    protected String type;
    protected int range;
    protected int number_of_attacks;
    protected int strength;
    protected int ap; /* armor piercing */
    protected Dice damage;
  
    @Override
    public int rollDamage(){
        return this.damage.roll();
    };
    @Override
        public int rollDamage(Dice d) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    };
  }
