public class SingleTargetDamage extends Effect{
    
    Dice damage_dice;
    
    public SingleTargetDamage(Dice d){
        super.target=null;
        this.damage_dice=d;
    }
    
    public SingleTargetDamage(Character c,Dice d){
        super.target=c;
        this.damage_dice=d;
    }
    
    @Override
    public void resolve(){
        int damage_rolled=this.rollDamage();
        this.target.isWounded(damage_rolled);
        System.out.println(this.target.name + " suffers " + damage_rolled + " points of damage!");
    };
    
    @Override
    public int rollDamage() {
        return super.rollDamage(this.damage_dice);
    }

    @Override
    public boolean hitRoll(Weapon weap) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean saveRoll(int mod) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
