public interface CombatInteraction {
    
    static Dice d6=new Dice(1,new int [] {1,2,3,4,5,6});
    static Dice double_d6=new Dice(2,new int [] {1,2,3,4,5,6});
    
    public void chooseTarget(Character c);
    public boolean hitRoll(Weapon weap);
    public boolean saveRoll(int mod);
}
