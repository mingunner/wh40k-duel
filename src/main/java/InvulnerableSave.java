public class InvulnerableSave extends Effect{

    int invuln_save;
    
    public InvulnerableSave(int value){
        this.invuln_save=value;
    }
    
    @Override
    public void resolve(){
        this.target.invul_save=this.invuln_save;
        this.target.temp_invul_save=this.invuln_save;
    }
    
    @Override
    public boolean hitRoll(Weapon weap) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean saveRoll(int mod) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int rollDamage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
