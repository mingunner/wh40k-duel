/**
 * @author mingunner
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Dice d3=new Dice(1,new int [] {1,2,3});
        Dice coin = new Dice(1,new int [] {1,2});
        boolean fairDuel = true;
        
        /* Effects */
        Reroll Reroll1Hits = new Reroll("hit",1);
        Reroll RerollHits = new Reroll("hit",6);
        Reroll RerollWounds = new Reroll("wound",6);
        InvulnerableSave IS4 = new InvulnerableSave(4);
        SingleTargetDamage d6Damage = new SingleTargetDamage(CombatOutput.d6);
        Sacrifice heroic = new Sacrifice();
        BonusAttack BAWound = new BonusAttack("fight","wound");
        BonusInvulnerability BInv1 = new BonusInvulnerability(1,3,5);
        
        /* Abilities and powers */
        Ability IronHalo = new Ability("Iron Halo", IS4);
        Ability RitesOfBattle = new Ability("Rites of Battle", Reroll1Hits);
        Ability MasterSwordman = new Ability("Master Swordman",RerollHits,RerollWounds,BAWound);
        Ability HeroicSacrifice = new Ability("Heroic Sacrifice",heroic);
        Power PurifyingFlame = new Power("Purifying Flame",5,"offensive",d6Damage);
        Power Sanctuary = new Power("Sanctuary",6,"buff",BInv1);
        
        /* Creating the character Captain Sicarius */
        int[] sicarius_sheet={6,2,2,4,4,5,4,9,2};
        Character Sicarius = new Character("Cato Sicarius",110,sicarius_sheet);
        MeleeWeapon tempestblade = new MeleeWeapon("Talassarian Tempest Blade",-3,d3);
        RangedWeapon artisan = new RangedWeapon("Artisan plasma pistol",12,"Pistol",1,8,-3,2);
        Sicarius.setEquip(tempestblade,artisan);
        Sicarius.setAbilities(IronHalo,RitesOfBattle);
        Sicarius.loadAbilities();
         
        /* Creating the character Castellan Crowe */
        int[] crowe_sheet={6,2,2,4,4,5,5,8,2};
        Character Crowe = new Character("Garran Crowe",80,crowe_sheet);
        MeleeWeapon blackblade = new MeleeWeapon("Black Blade of Antwyr",0,1);
        Crowe.setEquip(blackblade);
        Crowe.setAbilities(MasterSwordman,IronHalo,HeroicSacrifice);
        Crowe.loadAbilities();
        Crowe.setPowers(2, 1, PurifyingFlame,Sanctuary);
         
        /* Declaration of homicidal intents */
        Sicarius.chooseTarget(Crowe);
        Crowe.chooseTarget(Sicarius);
        
        /* Ten thousands battles between these legendary swordmasters */
        int winsSicarius=0;
        int winsCrowe=0;
        int draws=0;
        
        for (int i=0;i<10000;i++)
        {
        
        Sicarius.restore();
        Crowe.restore();
        
        /* Tossing a coin to see who fights first */
        Character attacker = null;
        Character defender = null;
        int flip=coin.roll();
        switch (flip){
            case 1:
            {
                System.out.println("Sicarius starts!");
                attacker=Sicarius;
                defender=Crowe;
                break;
            }
            case 2:
            {
                System.out.println("Crowe starts");
                attacker=Crowe;
                defender=Sicarius;
            }
        }
        
        /* combat start */
        int round=1;
        
        while (Sicarius.isAlive() && Crowe.isAlive()){
            System.out.println("ROUND "+round);
            
            /* True honour in battle is achieved in a clash of swords only */
            if (!fairDuel){
                
            /* Psychic phase */
            if (attacker.isPsyker()) {
                attacker.cancelBuffs(); /* buffs are temporary, glory is eternal */
                for (int j=0;j<attacker.manifest;j++)
                    if (defender.isAlive()) 
                        try {attacker.cast(attacker.powers_known[j]);}
                        catch (ArrayIndexOutOfBoundsException e)
                            {attacker.cast(attacker.powers_known[0]);} /* Usually it's some version of Smite, a spammable power */
            } 
            
            /* Shooting phase */
            if (attacker.equip[1] != null && "Pistol".equals(attacker.equip[1].type)) 
                attacker.attacks(attacker.equip[1]);
            }
            
            /* Fight/melee phase */
            attacker.attacks(attacker.equip[0]);
            if (defender.isAlive()){
                System.out.println(defender.name + " strikes back!");
                defender.attacks(defender.equip[0]);
                if ((! attacker.isAlive()) && attacker.sacrifice){
                    System.out.println(attacker.name + " won't die that easily!");
                    attacker.attacks(defender.equip[0]);
                }
            }
            else if (defender.sacrifice){
                    System.out.println("(SACRIFICE) "+defender.name + " won't die that easily!");
                    defender.attacks(defender.equip[0]);
                    }
            
            /* Switching attacker and defender */
            Character temp=attacker;
            attacker=defender;
            defender=temp;
            
            round++;
        } /* end while*/
        
        /* aftermath */
        Character winner = null;
        if (Sicarius.isAlive()) {
            winner=Sicarius;
            winsSicarius++;
        }
        else if (Crowe.isAlive()){
            winner=Crowe;
            winsCrowe++;
        }
        if (winner==null){
            System.out.println("Draw!");
            draws++;
        }
        else System.out.println(winner.name + " wins with "+winner.current_hp+ " hit points left!");
        
    } /* end for */
        
    System.out.println("Sicarius wins: "+winsSicarius);
    System.out.println("Crowe wins: "+winsCrowe);
    System.out.println("Draws: "+draws);
    
    } /* end main method */
    
}
