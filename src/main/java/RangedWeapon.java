public class RangedWeapon extends Weapon{
    
    /* constant damage */
    public RangedWeapon(String nm, int rng, String tp, int noa, int str, int ap, int dmg){
        super.name=nm;
        super.range=rng;
        this.type=tp;
        super.number_of_attacks=noa;
        super.strength=str;
        super.ap=ap;
        super.damage=new Dice(dmg);
    };

    /* random damage */
    public RangedWeapon(String nm, int rng, String tp, int noa, int str, int ap, Dice dmg){
        super.name=nm;
        super.range=rng;
        this.type=tp;
        super.number_of_attacks=noa;
        super.strength=str;
        super.ap=ap;
        super.damage=dmg;
    };
}
