public class Ability {
    
    String name;
    Effect [] effects;
    
    public Ability(String nm, Effect... args){
        this.name=nm;
        this.effects=args;
    }
}
