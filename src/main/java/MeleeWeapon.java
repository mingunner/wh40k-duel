public class MeleeWeapon extends Weapon{
 
    int strength_multiplier;
    int strength_bonus;
    
    /*constant damage*/
    public MeleeWeapon(String nm,int ap, int dmg,int str_mp, int str_b){ 
        super.name=nm;
        super.type="Melee";
        super.range=0;
        super.number_of_attacks=1;
        super.ap=ap;
        super.damage=new Dice(dmg);
        this.strength_multiplier=str_mp;
        this.strength_bonus=str_b;
    };
    
    public MeleeWeapon(String nm,int ap, int dmg){
        this(nm,ap,dmg,1,0);
    };
    
    public MeleeWeapon(){
        this("Basic melee",0,1);
    }
    
    /* random damage */
    public MeleeWeapon(String nm,int ap, Dice dmg,int str_mp, int str_b){ 
        super.name=nm;
        super.type="Melee";
        super.range=0;
        super.number_of_attacks=1;
        super.ap=ap;
        super.damage=dmg;
        this.strength_multiplier=str_mp;
        this.strength_bonus=str_b;
    };
  
    public MeleeWeapon(String nm,int ap, Dice dmg){
        this(nm,ap,dmg,1,0);
    };
}
