public abstract class Effect implements CombatInteraction, CombatOutput{
    
    Character target;
    
    public void resolve(){};
    public void cancel(){}
   
    /* Effect1.chain(Effect2).chain(Effect3).resolve() will resolve
    the effects in the given order */
    public Effect chain(Effect eff){ /* actually never used */
        this.resolve();
        return eff;
    }
   
    @Override
    public void chooseTarget(Character c){
        this.target=c;
    }
   
    @Override
    public int rollDamage(Dice d){
        return d.roll();
    }
}
