public class Power {
    
    String name;
    int warp_charge;
    String category; /* offensive or buff, other kinds are ignored in this simulation */
    Effect effect;
    SingleTargetDamage perils = new SingleTargetDamage(CombatOutput.d3); /* miscasting the power is dangerous */
    
    public Power(String name, int charge, String categ, Effect effect){
        this.name=name;
        this.warp_charge=charge;
        this.category=categ;
        this.effect=effect;
    }
}
