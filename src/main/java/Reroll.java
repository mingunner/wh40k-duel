public class Reroll extends Effect{

    String what;
    int value;
    
    public Reroll(String what, int value){
        this.what=what; /* hit or wound, other rolls are not contemplated */
        this.value=value; /* 1 or 6, other values are not contemplated  */
    }
    
    @Override
    public void resolve(){
        if ("hit".equals(this.what)) {
            if (this.value == 1) this.target.reroll1hit = true;
            else if (this.value == 6) this.target.rerollhit = true;
        }
        else if ("wound".equals(this.what)) {
            if (this.value == 1) this.target.reroll1wound = true;
            else if (this.value == 6) this.target.rerollwound = true;
    }
    }
    
    @Override
    public boolean hitRoll(Weapon weap) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean saveRoll(int mod) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int rollDamage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
