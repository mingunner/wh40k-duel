This is a project of mine made as an exercise for learning java. Lots of assumptions have been made and it needs sanitization and commenting but I don't plan to update it further.

The project is based on the 8th edition of Warhammer 40000 tabletop game. It takes two characters, Captain Sicarius and Castellan Crowe, two famous swordsmen in this setting, and makes them fight following the rules.

Ten thousands duels are simulated and total results are printed at the end.

Characters are supposed to be in front of each other, so there is no code about distance, cover and positions, plus the only ranged weapon described has the Pistol type because it can be used in melee range, unlike rifles or grenades.
